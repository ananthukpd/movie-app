from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:9645914112@localhost/flaskdb'
db = SQLAlchemy(app)
bootstrap = Bootstrap(app)

class MovieTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), nullable=False)
    director = db.Column(db.String(50), nullable=False)
    synopsis = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return "movie saved"  


@app.route('/')
def hello():
    return render_template("home.html")

@app.route('/register', methods=['GET', 'POST'])
def registerMovie():
    if  request.method == 'POST':
        movie = request.form['movie']
        director = request.form['director']
        synopsis = request.form['synopsis']
        print(movie,director,synopsis)
        movieDetail = MovieTable(title=movie, director=director, synopsis=synopsis)
        db.session.add(movieDetail)
        db.session.commit()

        return render_template("home.html",messege="Your Review is Saved")
    else:
        return "unsucsessful"

@app.route('/posts')
def posts():
    posts = MovieTable.query.order_by(MovieTable.title).all()
    return render_template("tableview.html", allposts=posts)

@app.route('/home')
def home():
    return render_template("home.html")

@app.route('/posts/delete/<int:id>')
def postDel(id):
    posts = MovieTable.query.get_or_404(id)
    db.session.delete(posts)
    db.session.commit()
    return redirect('/posts')

@app.route('/posts/edit/<int:id>', methods=['GET', 'POST'])
def editMov(id):
    if  request.method == 'POST':
        movie = request.form['movie']
        director = request.form['director']
        synopsis = request.form['synopsis']
        print(movie,director,synopsis)
        movieDetail = MovieTable(title=movie, director=director, synopsis=synopsis)
        db.session.add(movieDetail)
        db.session.commit()

        return render_template("edit.html",messege="Your Review is Saved")
    else:
        editPost = MovieTable.query.get_or_404(id)
        return render_template("edit.html",posts=editPost)

if __name__ == "__main__":
    app.run(debug=True) 